#ifndef _GAME_H_
#define _GAME_H_
#include <windows.h>
#include <d3d9.h>
#include <dinput.h>

#define GAME_SCREEN_RESOLUTION_320_240_24   0
#define GAME_SCREEN_RESOLUTION_640_480_24   1
#define GAME_SCREEN_RESOLUTION_900_640_24   2
#define GAME_SCREEN_RESOLUTION_1024_768_24  3

#define GAME_SCREEN_RESOLUTION_320_240_32   10
#define GAME_SCREEN_RESOLUTION_640_480_32   11
#define GAME_SCREEN_RESOLUTION_800_600_32   12
#define GAME_SCREEN_RESOLUTION_1024_768_32  13

#define KEY_DOWN(code) ( IsKeyDown(code) )

#define KEYBOARD_BUFFER_SIZE	1024

struct VIEWPORT
{
	int _Width;		// Height of view port
	int _Height;		// Width of view port

	int _X;			// Position of view port in World space
	int _Y;
};

class CGame 
{
protected:
protected:
	LPDIRECT3D9 _d3d;
	LPDIRECT3DDEVICE9 _d3ddv;		// Rendering device
	LPDIRECT3DSURFACE9 _BackBuffer;	// Pointer to backbuffer 

	LPDIRECTINPUT8       _di;		// The DirectInput object         
	LPDIRECTINPUTDEVICE8 _Keyboard;	// The keyboard device 

	BYTE  _KeyStates[256];			// DirectInput keyboard state buffer 

									// Buffered keyboard data
	DIDEVICEOBJECTDATA _KeyEvents[KEYBOARD_BUFFER_SIZE];

	DWORD _DeltaTime;		// Time between the last frame and current frame
	int _Mode;				// Screen mode 
	int _IsFullScreen;		// Is running in fullscreen mode?
	int _FrameRate;

	int _ScreenWidth;
	int _ScreenHeight;
	int _Depth;

	D3DFORMAT _BackBufferFormat;

	HINSTANCE _hInstance;	// Handle of the game instance
	HWND _hWnd;				// Handle of the Game Window

	LPWSTR _Name;			// Name of game will be used as Window Class Name

	void SetScreenDimension(int Mode);

	static LRESULT CALLBACK _WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	void InitWindow();
	void InitDirectX();
	void InitInputDevice();

	// Render a single frame
	void _RenderFrame();

	void ProcessKeyBoard();
	int IsKeyDown(int KeyCode);

	//
	// Place holder for child classes
	//
	virtual void UpdateWorld(int Delta);
	virtual void RenderFrame(LPDIRECT3DDEVICE9 d3ddv, int Delta);
	virtual void LoadResources(LPDIRECT3DDEVICE9 d3ddv);
	virtual void ProcessInput(LPDIRECT3DDEVICE9 d3ddv, int Delta);

	virtual void OnKeyDown(int KeyCode);
	virtual void OnKeyUp(int KeyCode);

public:
	LPDIRECT3D9 GetDirectX();
	LPDIRECT3DDEVICE9 GetDevice();
	LPDIRECT3DSURFACE9 GetBackBuffer();

	int GetMode();

	int GetScreenWidth();
	int GetScreenHeight();
	int GetDepth();

	CGame(HINSTANCE hInstance, LPWSTR Name, int Mode, int IsFullscreen, int FrameRate);
	~CGame();

	// Initialize the game with set parameters
	void Init();

	// Run game
	void Run();
};

#endif