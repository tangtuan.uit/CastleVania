#ifndef _STATE_H
#define _STATE_H

#include <d3d9.h>
#include <d3dx9.h>
#include <fstream>
#include <stdio.h>
#include <string>
#include "CGame.h"

#define BACKGROUND_CELL_SIZE 32

using namespace std;
class STATE
{
private:
	LPDIRECT3DDEVICE9 _d3ddv;
	LPD3DXSPRITE _SpriteHandler;
	int _Cols;
	int _Rows;
	int *_MapSet;

public:
	int _X;
	int _Y;
	int _Width;
	int _Height;

	STATE::STATE(int X, int Y, int Col, int Row);
	void Load(ifstream &file);

	bool CheckIn(int x, int y);

	void Render(VIEWPORT ViewPort, LPDIRECT3DTEXTURE9 BgTexture);
	~STATE();
};

#endif