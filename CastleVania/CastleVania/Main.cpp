#include <windows.h>
#include "CastleVania.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	CastleVania game(hInstance, L"Castle Vania", GAME_SCREEN_RESOLUTION_640_480_24, 0, 60);

	game.Init();
	game.Run();

	return 0;
}