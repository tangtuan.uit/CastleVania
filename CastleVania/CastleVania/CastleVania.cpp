#include <time.h>
#include <d3dx9.h>
#include "CastleVania.h"
//#include "QuadTree.h"

LPDIRECT3DTEXTURE9 MiscTexture;
LPDIRECT3DTEXTURE9 EnemyTexture;
LPDIRECT3DTEXTURE9 BossTexture;
//vector<G_OBJECT *> lisObj;
//vector<ITEM *> lisItem;
//vector<MISC *> lisMisc;
//SIMON * simon;
//STATE * currentState;
//QNODE * root_node;
int Game_Point;

bool stop_watch_active;
bool rosary_active;

extern LPDIRECT3DDEVICE9 DDevice;
extern VIEWPORT ViewPort;
extern bool Is_ViewPort_Control;


CastleVania::CastleVania(HINSTANCE hInstance, LPWSTR Name, int Mode, int IsFullScreen, int FrameRate) :
	CGame(hInstance, Name, Mode, IsFullScreen, FrameRate)
{
	//wrl = NULL;
	Game_Point = 0;
	Is_ViewPort_Control = true;
	stop_watch_active = false;
	rosary_active = false;
}

CastleVania::~CastleVania()
{
/*	for each (G_OBJECT * go in lisObj)
		delete go;
	lisObj.clear();
	delete wrl*/;
}

void CastleVania::LoadResources(LPDIRECT3DDEVICE9 d3ddv)
{
	//MiscTexture = LoadTexture(MISC_IMAGE_PATH);
	//EnemyTexture = LoadTexture(ENEMY_IMAGE_PATH);
	//BossTexture = LoadTexture(BOSS_IMAGE_PATH);

	//wrl = new WORLDMAP();
	//wrl->LoadMap();
	//currentState = wrl->GetAllState()[0];
	//simon = new SIMON(100, 64, 10);
	//lisObj = wrl->GetAllObject();
}

LPDIRECT3DTEXTURE9 CastleVania::LoadTexture(LPWSTR TextureDir)
{
	LPDIRECT3DTEXTURE9 _Txture;
	D3DXIMAGE_INFO info;
	HRESULT hr = D3DXGetImageInfoFromFile(TextureDir, &info);

	hr = D3DXCreateTextureFromFileEx(
		DDevice,
		TextureDir,
		info.Width,
		info.Height,
		1,
		D3DUSAGE_DYNAMIC,
		D3DFMT_UNKNOWN,
		D3DPOOL_DEFAULT,
		D3DX_DEFAULT,
		D3DX_DEFAULT,
		D3DCOLOR_XRGB(0, 255, 0),
		&info,
		NULL,
		&_Txture);
	return _Txture;
}

void CastleVania::UpdateWorld(int t)
{
	/*simon->Update(t);

#pragma region ITEM
	for each (ITEM * item in lisItem)
	{
		item->Update(_DeltaTime);
		if (simon->CollidedWith(item) && item->Live_State == ALIVE)
		{
			simon->PickUpItem(item);
			item->Live_State = DESTROYED;
		}

	}
#pragma endregion

#pragma region STATE
	for each (STATE * st in wrl->GetAllState())
	{
		if (st->CheckIn(simon->_X + simon->_Width, simon->_Y))
		{
			currentState = st;
			break;
		}
	}
	if (simon->_X < wrl->GetAllState()[0]->_X)
		simon->_X = wrl->GetAllState()[0]->_X;*/

#pragma endregion

#pragma region VIEWPORT
	UpdateViewPort();

#pragma endregion

#pragma region OBJECT
	RECT cam_rect = { ViewPort._X - 32, ViewPort._Y - 32, ViewPort._X + ViewPort._Width + 64, ViewPort._Y + ViewPort._Height + 64 };

	/*lisObj = root_node->Query(cam_rect);
	for each (G_OBJECT * obj in lisObj)
	{
		obj->Update(t);
		if (obj->_Type == OBJ_GR_BRICK || obj->_Type == OBJ_GR_HIDDEN_BRICK ||
			obj->_Type == OBJ_GR_MOVING_BRICK)
		{
			int co = simon->CollidedWith(obj);
			if (co)
			{
				simon->FixPositionCollid(obj, co);
				if (co == COLLIDED_BOT)
					simon->Land(obj);
			}
		}

		if (obj->_Type == OBJ_GR_DOOR_1 || obj->_Type == OBJ_GR_DOOR_2)
		{
			int co = simon->CollidedWith(obj);
			if (co)
			{
				if (!((DOOR*)obj)->IsOpened())
					((DOOR*)obj)->Open();
				else
					simon->FixPositionCollid(obj, co);
			}
		}

		if (obj->_Type == OBJ_GR_STAIR_UP || obj->_Type == OBJ_GR_STAIR_DOWN)
		{
			if (simon->CollidedWith(((STAIR*)obj)->StairIn()) != COLLIDED_NONE)
			{
				simon->CheckStair((STAIR*)obj, 1);
			}
			else if (simon->CollidedWith(((STAIR*)obj)->StairOut()) != COLLIDED_NONE)
				simon->CheckStair((STAIR*)obj, -1);
		}
	}

	for each (MISC * misc in lisMisc)
		misc->Update(t);*/
#pragma endregion
}

void CastleVania::UpdateViewPort()
{
	if (!Is_ViewPort_Control)
		return;

	//if (currentState != NULL)
	//{
	//	//ViewPort._X = simon->_X + simon->_Width - ViewPort._Width / 2;

	//	if (ViewPort._X < currentState->_X)
	//		ViewPort._X = currentState->_X;
	//	if (ViewPort._X + ViewPort._Width > currentState->_X + currentState->_Width)
	//		ViewPort._X = currentState->_X + currentState->_Width - ViewPort._Width;

	//	ViewPort._Y = currentState->_Y;
	//}
}

void CastleVania::RenderFrame(LPDIRECT3DDEVICE9 d3ddv, int t)
{
	if (_d3ddv->BeginScene())
	{
		_d3ddv->ColorFill(_BackBuffer, NULL, D3DCOLOR_XRGB(0, 0, 0));

		/*if (currentState != NULL)
			wrl->Render();*/

		/*for each (G_OBJECT * obj in lisObj)
		{
			if (obj->_Type == OBJ_GR_FIRE_CANDLE || obj->_Type == OBJ_GR_FIRE_TOWER ||
				obj->_Type == OBJ_GR_DOOR_1 || obj->_Type == OBJ_GR_DOOR_2 ||
				obj->_Type == OBJ_GR_HIDDEN_BRICK || obj->_Type == OBJ_GR_MOVING_BRICK)
				obj->Render();
		}
		for each (G_OBJECT * obj in lisObj)
		{
			if (obj->_Type == OBJ_EN_ZOMBIE || obj->_Type == OBJ_EN_PANTHER ||
				obj->_Type == OBJ_EN_BAT || obj->_Type == OBJ_EN_MERMAN)
				obj->Render();
		}

		for each (ITEM * item in lisItem)
			item->Render();

		simon->Render();

		for each (MISC * misc in lisMisc)
			misc->Render();

		for each (G_OBJECT * obj in lisObj)
			if (obj->_Type == OBJ_GR_GI_CASTLE)
				obj->Render();*/

		RECT a = { 0, 0, 640, 480 - 368 };
		RECT b = { 0, 480 - 16, 640, 480 };

		_d3ddv->ColorFill(_BackBuffer, &a, D3DCOLOR_XRGB(0, 0, 0));
		_d3ddv->ColorFill(_BackBuffer, &b, D3DCOLOR_XRGB(0, 0, 0));
		_d3ddv->EndScene();
	}
	_d3ddv->Present(NULL, NULL, NULL, NULL);

}

void CastleVania::ProcessInput(LPDIRECT3DDEVICE9 d3ddv, int t)
{
	bool up = false;
	bool down = false;
	bool left = false;
	bool right = false;

	if (IsKeyDown(DIK_NUMPAD8)) up = true;
	if (IsKeyDown(DIK_NUMPAD5)) down = true;
	if (IsKeyDown(DIK_NUMPAD4)) left = true;
	if (IsKeyDown(DIK_NUMPAD6)) right = true;

	//simon->TriggerControl(up, down, left, right);
}

void CastleVania::OnKeyDown(int KeyCode)
{
	//switch (KeyCode)
	//{
	//case DIK_SPACE:
	//	simon->Jump();			// start jump if is not "on-air"
	//	break;
	//case DIK_Z:
	//	simon->Attack();
	//	break;
	//case DIK_A:
	//	simon->Damaged(2, FACING_RIGHT);
	//	break;
	//case DIK_1:
	//	simon->Cheat(1);
	//	break;
	//case DIK_2:
	//	simon->Cheat(2);
	//	break;
	//case DIK_3:
	//	simon->Cheat(3);
	//	break;
	//}
}