#include "State.h"

extern LPDIRECT3DDEVICE9 DDevice;

STATE::STATE(int X, int Y, int Col, int Row)
{
	_d3ddv = DDevice;
	D3DXCreateSprite(_d3ddv, &_SpriteHandler);
	_X = X;
	_Y = Y;
	_Width = Col* BACKGROUND_CELL_SIZE;
	_Height = Row* BACKGROUND_CELL_SIZE;
	_Cols = Col;
	_Rows = Row;
	_MapSet = new int[_Cols * _Rows];
}

void STATE::Load(ifstream &file)
{
	string data;
	getline(file, data);
	for (int i = 0; i < _Cols * _Rows; i++) {
		file >> _MapSet[i];
	}
	getline(file, data);
}

bool STATE::CheckIn(int x, int y)
{
	if (x >= _X && x < _X + _Width &&
		y >= _Y && y < _Y + _Height)
		return true;
	else
		return false;
}

void STATE::Render(VIEWPORT ViewPort, LPDIRECT3DTEXTURE9 BgTexture)
{
	int Col_str = max((ViewPort._X - _X) / BACKGROUND_CELL_SIZE - 1, 0);
	int Row_str = max((_Y - ViewPort._Y) / BACKGROUND_CELL_SIZE, 0);

	int Col_offset = min((ViewPort._X - _X + ViewPort._Width) / BACKGROUND_CELL_SIZE + 2, _Cols);
	int Row_offset = min((_Y - ViewPort._Y + ViewPort._Height) / BACKGROUND_CELL_SIZE, _Rows);

	_SpriteHandler->Begin(D3DXSPRITE_ALPHABLEND);

	D3DXVECTOR2 center((float)(_X + _Width / 2), (float)(_Y + _Height / 2));
	D3DXVECTOR2 scaling = D3DXVECTOR2(1.0f, -1.0f);
	D3DXMATRIX mat;
	D3DXMatrixTransformation2D(&mat, &center, 0.0f, &scaling, NULL, 0.0f, NULL);

	D3DXMATRIX matTmp;
	D3DXMatrixTranslation(&matTmp, -(float)ViewPort._X, (float)(-ViewPort._Y - 464), 0.0f);
	mat *= matTmp;
	D3DXMatrixScaling(&matTmp, 1.0f, -1.0f, 1.0f);
	mat *= matTmp;

	_SpriteHandler->SetTransform(&mat);

	for (int i = Row_str; i < Row_offset; i++) {
		for (int j = Col_str; j < Col_offset; j++) {

			int index = _MapSet[i * _Cols + j];

			RECT srect;
			srect.left = (index % 20) * BACKGROUND_CELL_SIZE;
			srect.top = (index / 20) * BACKGROUND_CELL_SIZE;
			srect.right = srect.left + BACKGROUND_CELL_SIZE;
			srect.bottom = srect.top + BACKGROUND_CELL_SIZE;

			D3DXVECTOR3 position(float(_X + j * BACKGROUND_CELL_SIZE), float(_Y + i * BACKGROUND_CELL_SIZE), 0.0f);

			_SpriteHandler->Draw(
				BgTexture,
				&srect,
				NULL,
				&position,
				D3DCOLOR_XRGB(255, 255, 255)
			);
		}
	}

	_SpriteHandler->End();
}