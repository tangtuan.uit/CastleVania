#ifndef _WORLDMAP_H
#define _WORLDMAP_H

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include <d3dx9.h>
#include <stdio.h>

#include "State.h"

class WORLDMAP
{
private:
	LPDIRECT3DDEVICE9 _d3ddv;

	vector<STATE * > _ListState;
	//vector<G_OBJECT * > _ListObj;
	LPDIRECT3DTEXTURE9 BgTexture;

	LPSTR mapDir;
	LPWSTR tilesDir;

public:
	WORLDMAP::WORLDMAP();
	//void LoadResource();
	void LoadMap();
	void Render();
	void RenderCrState(VIEWPORT ViewPort, STATE * state);

	//void AddObject(int type, int item, int x, int y, int var1, int var2, int var3);

	vector<STATE *> GetAllState();
	//vector<G_OBJECT *> GetAllObject();

	~WORLDMAP();
};

#endif