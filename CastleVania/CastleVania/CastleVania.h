#ifndef _CASTLEVANIA_H_
#define _CASTLEVANIA_H_

#include <d3dx9.h>
//#include "WorldMap.h"
#include "CGame.h"

//#include "BreakObject.h"

class CastleVania : public CGame
{
public:
	CastleVania(HINSTANCE hInstance, LPWSTR Name, int Mode, int IsFullScreen, int FrameRate);
	~CastleVania();

	//WORLDMAP * wrl;
protected:

	virtual void UpdateWorld(int Delta);
	virtual void RenderFrame(LPDIRECT3DDEVICE9 d3ddv, int Delta);
	virtual void ProcessInput(LPDIRECT3DDEVICE9 d3ddv, int Delta);
	virtual void LoadResources(LPDIRECT3DDEVICE9 d3ddv);
	LPDIRECT3DTEXTURE9 LoadTexture(LPWSTR TextureDir);

	virtual void UpdateViewPort();

	virtual void OnKeyDown(int KeyCode);
};

#endif