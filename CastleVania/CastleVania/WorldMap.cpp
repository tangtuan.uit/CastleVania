﻿#include "WorldMap.h"
#include "CGame.h"

extern LPDIRECT3DDEVICE9 DDevice;
//extern QNODE * root_node;
extern VIEWPORT ViewPort;

using namespace std;
WORLDMAP::WORLDMAP()
{
	_d3ddv = DDevice;
	BgTexture = NULL;
	mapDir = "Map//Level 1//State 2//TileMap.txt";
	tilesDir = L"Map//Level 1//State 2//TileMap.png";
}

void WORLDMAP::LoadMap()
{
	D3DXIMAGE_INFO info;
	HRESULT hr = D3DXGetImageInfoFromFile(tilesDir, &info);

	hr = D3DXCreateTextureFromFileEx(
		_d3ddv,
		tilesDir,
		info.Width,
		info.Height,
		1,
		D3DUSAGE_DYNAMIC,
		D3DFMT_UNKNOWN,
		D3DPOOL_DEFAULT,
		D3DX_DEFAULT,
		D3DX_DEFAULT,
		D3DCOLOR_XRGB(0, 0, 0),
		&info,
		NULL,
		&BgTexture);

	ifstream file(mapDir);
	string data;
	getline(file, data); // HEADER
	getline(file, data); // Map Info
	getline(file, data); // State_Num	Tiles_Num	Tiles_pRow	Object_Num

	int state_num; file >> state_num;
	int tiles_num; file >> tiles_num;
	getline(file, data); // fin

	getline(file, data); // Space
	getline(file, data); // State Info
	getline(file, data); // State 1

	for (int i = 0; i < state_num; i++)
	{
		int x; file >> x;
		int y; file >> y;
		int c; file >> c;
		int r; file >> r;
		STATE * st = new STATE(x, y, c, r);
		_ListState.push_back(st);
	}

	getline(file, data); // fin
	getline(file, data); // END_HEADER
	getline(file, data); // Space
	getline(file, data); // STATE_SET

	for each (STATE * st in _ListState)
	{
		st->Load(file);
	}

	getline(file, data); // END_STATE_SET
	//getline(file, data); // Space
	//getline(file, data); // OBJECT_LIST
	//getline(file, data); // (...)

}

void WORLDMAP::RenderCrState(VIEWPORT ViewPort, STATE * state)
{
	state->Render(ViewPort, BgTexture);
}

void WORLDMAP::Render()
{
	for each (STATE * st in _ListState)
	{
		st->Render(ViewPort, BgTexture);
	}
}

vector<STATE *> WORLDMAP::GetAllState()
{
	return _ListState;
}

//vector<G_OBJECT *> WORLDMAP::GetAllObject()
//{
//	return _ListObj;
//}

WORLDMAP::~WORLDMAP()
{
	BgTexture->Release();
}